<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,500,700|Roboto:300,400,500,700" rel="stylesheet">

    <link href="{{ mix('/css/vue-styles.css') }}" rel="stylesheet">
    <script>
        window.apiClient = {
            clientId: 2,
            clientSecret: 'CbIXwHAq3yobcyGaqiM8LMZpOVZZjfchrhINiTqP'
        };
    </script>
</head>

<body>
    <div id="app"></div>
    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>