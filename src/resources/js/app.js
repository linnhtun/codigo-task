import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.use(VueMaterial);

Vue.filter('toCurrency', function(value) {
  if (Number(value) === NaN) {
    return value;
  }
  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
  });
  return formatter.format(value);
});

require('./bootstrap');

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
