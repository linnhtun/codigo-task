import HomeComponent from '../components/HomeComponent.vue';
import CheckoutComponent from '../components/CheckoutComponent.vue';
import OrderComponent from '../components/OrderComponent.vue';

export default [
  { path: '/', component: HomeComponent },
  { path: '/checkout', component: CheckoutComponent },
  { path: '/order', component: OrderComponent },
];
