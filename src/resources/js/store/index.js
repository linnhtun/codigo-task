import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import classPacks from './modules/classPacks';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    classPacks,
  },
  strict: true,
});
