import api from '../../api';

const state = {
  token: null,
};

const mutations = {
  setUser(s, data) {
    s.token = data;
  },
};

const getters = {
  getLoginUser(state) {
    return state.token;
  },
};

const actions = {
  doLogin({ state, commit, dispatch }, { email, password }) {
    api.login(email, password).then(({ data }) => {
      commit('setUser', data);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
