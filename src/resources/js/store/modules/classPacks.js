import api from '../../api';

const state = {
  classPacks: {},
  promo: {},
  order: {},
  selected: {
  },
};

const mutations = {
  setClassPacks(s, data) {
    s.classPacks = data;
  },
  setSelectClassPack(s, classPack) {
    s.selected = classPack;
  },
  setPromo(s, promo) {
    s.promo = promo;
  },
  setOrder(s, order) {
    s.order = order;
  }
};

const getters = {
  getClassPacks(state) {
    return state.classPacks.pack_list;
  },
  getSelectedClassPack(state) {
    return state.selected;
  },
  getTier(state) {
    return state.classPacks.tier;
  },
  getPromo(state) {
    return state.promo;
  },
};

const actions = {
  retrievePaginated({ commit, rootState }) {
    api.retrievePaginated(rootState.user.token).then(({ data }) => {
      commit('setClassPacks', data.data);
    });
  },
  selectClassPack({ commit }, classPack) {
    commit('setSelectClassPack', classPack);
  },
  applyPromo({ commit }, code) {
    api.applyPromo(code).then(({ data }) => {
      commit('setPromo', data);
    });
  },
  doOrder({state, commit}) {
    api.recordOrder(state.selected, state.promo).then(({ data }) => {
      commit('setOrder', data);
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
