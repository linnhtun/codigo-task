import axios from 'axios';

export default {
  async login(email, password) {
    let bodyFormData = new FormData();
    bodyFormData.set('grant_type', 'password');
    bodyFormData.set('client_id', window.apiClient.clientId);
    bodyFormData.set('client_secret', window.apiClient.clientSecret);
    bodyFormData.set('username', email);
    bodyFormData.set('password', password);
    bodyFormData.set('scope', '');

    return axios.post('http://localhost/oauth/token', bodyFormData);
  },
  async retrievePaginated(token) {
    return axios.get('http://localhost/api/classpacks', {
      headers: {
        // Authorization: 'Bearer ' + token.access_token,
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiOWVlYWJhNjhkMDgwM2YyOWNjNmRkN2JkZWE3YTdlZTQ4OWMyZjIxNDlhNjVlZTA3NDQxOTg2MzYxYmQzNjNlNTU2MTUyYWU2OGM3YzNmMWQiLCJpYXQiOjE1NzI0Njc2NTMsIm5iZiI6MTU3MjQ2NzY1MywiZXhwIjoxNjA0MDkwMDUzLCJzdWIiOiIxMCIsInNjb3BlcyI6W119.I_iL4ruTt9yz8tQok9SYVKdvUoLqBfKgklvxPU2quhZQkC4khwmnwp7cYEWCM6hk1DQ-NNHP5pFfPV4nLZt6iUmiRt86XhqWSqcOBt0vSBbzlL-Xfg7FaBt0ios2Mprgzo0bLqp6gLFrzuCRA04BebewK7sVqKnEH9DNpZM78l-kx-RoKfFKe3bwy-mBB-TYI9l4OMANfFtLSFZ9ND3JlqzOjhl5d15Xkj6L2dUg_IjklVRCaidu6t1tXdzS6lmGeF7rZaMPOWs5SfzLXXV5Wu-4IdLKOs5DyDEXVmGwmeea4jx4wDaSyiracC7edufr9KamtEcHs6Ee-dx9tMedHeMZspXENDRO6hBq0AwUTuuhOk4fqE1YRRW3ZsGt4lZSaYixDsubMWRHpf92pkCMal-HHqevuGuZGsUvuwcLJExNzMfYLAw7aLyi5olC_CkwAZiELCtiHpKIebNh8QqIOUQZCdmXqiyd6w2cyI-45prFCcfxndE4n2qLtKhOZ6TTi-iVBDrW6yCr03pZ7mHeOMfolKzqoxCfXJ-WZG1aFl6WygFbK39Zg2cNPfE2jRNyTOtVcdlVxUxWRf8ewHO4MUDIVaYMBAEeBoNHtXtM5wMNu2zZcS2WzqcooVqvMheSNMVvro_NA-chzZ59Pp2EBSfn3cSVH8z_753qmt9Yi84',
      },
    });
  },
  async applyPromo(code) {
    return axios.get('http://localhost/api/promotions?code=' + code, {
      headers: {
        // Authorization: 'Bearer ' + token.access_token,
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiOWVlYWJhNjhkMDgwM2YyOWNjNmRkN2JkZWE3YTdlZTQ4OWMyZjIxNDlhNjVlZTA3NDQxOTg2MzYxYmQzNjNlNTU2MTUyYWU2OGM3YzNmMWQiLCJpYXQiOjE1NzI0Njc2NTMsIm5iZiI6MTU3MjQ2NzY1MywiZXhwIjoxNjA0MDkwMDUzLCJzdWIiOiIxMCIsInNjb3BlcyI6W119.I_iL4ruTt9yz8tQok9SYVKdvUoLqBfKgklvxPU2quhZQkC4khwmnwp7cYEWCM6hk1DQ-NNHP5pFfPV4nLZt6iUmiRt86XhqWSqcOBt0vSBbzlL-Xfg7FaBt0ios2Mprgzo0bLqp6gLFrzuCRA04BebewK7sVqKnEH9DNpZM78l-kx-RoKfFKe3bwy-mBB-TYI9l4OMANfFtLSFZ9ND3JlqzOjhl5d15Xkj6L2dUg_IjklVRCaidu6t1tXdzS6lmGeF7rZaMPOWs5SfzLXXV5Wu-4IdLKOs5DyDEXVmGwmeea4jx4wDaSyiracC7edufr9KamtEcHs6Ee-dx9tMedHeMZspXENDRO6hBq0AwUTuuhOk4fqE1YRRW3ZsGt4lZSaYixDsubMWRHpf92pkCMal-HHqevuGuZGsUvuwcLJExNzMfYLAw7aLyi5olC_CkwAZiELCtiHpKIebNh8QqIOUQZCdmXqiyd6w2cyI-45prFCcfxndE4n2qLtKhOZ6TTi-iVBDrW6yCr03pZ7mHeOMfolKzqoxCfXJ-WZG1aFl6WygFbK39Zg2cNPfE2jRNyTOtVcdlVxUxWRf8ewHO4MUDIVaYMBAEeBoNHtXtM5wMNu2zZcS2WzqcooVqvMheSNMVvro_NA-chzZ59Pp2EBSfn3cSVH8z_753qmt9Yi84',
      },
    });
  },
  async recordOrder(pack, promo) {
    let bodyFormData = new FormData();
    bodyFormData.set('pack_id', pack.id);
    if (promo.id) {
        bodyFormData.set('promotion_id', promo.id);
    }
    return axios.post('http://localhost/api/order', bodyFormData, {
      headers: {
        // Authorization: 'Bearer ' + token.access_token,
        Authorization:
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiOWVlYWJhNjhkMDgwM2YyOWNjNmRkN2JkZWE3YTdlZTQ4OWMyZjIxNDlhNjVlZTA3NDQxOTg2MzYxYmQzNjNlNTU2MTUyYWU2OGM3YzNmMWQiLCJpYXQiOjE1NzI0Njc2NTMsIm5iZiI6MTU3MjQ2NzY1MywiZXhwIjoxNjA0MDkwMDUzLCJzdWIiOiIxMCIsInNjb3BlcyI6W119.I_iL4ruTt9yz8tQok9SYVKdvUoLqBfKgklvxPU2quhZQkC4khwmnwp7cYEWCM6hk1DQ-NNHP5pFfPV4nLZt6iUmiRt86XhqWSqcOBt0vSBbzlL-Xfg7FaBt0ios2Mprgzo0bLqp6gLFrzuCRA04BebewK7sVqKnEH9DNpZM78l-kx-RoKfFKe3bwy-mBB-TYI9l4OMANfFtLSFZ9ND3JlqzOjhl5d15Xkj6L2dUg_IjklVRCaidu6t1tXdzS6lmGeF7rZaMPOWs5SfzLXXV5Wu-4IdLKOs5DyDEXVmGwmeea4jx4wDaSyiracC7edufr9KamtEcHs6Ee-dx9tMedHeMZspXENDRO6hBq0AwUTuuhOk4fqE1YRRW3ZsGt4lZSaYixDsubMWRHpf92pkCMal-HHqevuGuZGsUvuwcLJExNzMfYLAw7aLyi5olC_CkwAZiELCtiHpKIebNh8QqIOUQZCdmXqiyd6w2cyI-45prFCcfxndE4n2qLtKhOZ6TTi-iVBDrW6yCr03pZ7mHeOMfolKzqoxCfXJ-WZG1aFl6WygFbK39Zg2cNPfE2jRNyTOtVcdlVxUxWRf8ewHO4MUDIVaYMBAEeBoNHtXtM5wMNu2zZcS2WzqcooVqvMheSNMVvro_NA-chzZ59Pp2EBSfn3cSVH8z_753qmt9Yi84',
      },
    });
  },
};
