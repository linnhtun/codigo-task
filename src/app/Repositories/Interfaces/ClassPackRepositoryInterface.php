<?php

namespace App\Repositories\Interfaces;

interface ClassPackRepositoryInterface
{
    /**
     * Get's all class packs.
     *
     * @return mixed
     */
    public function all();
}
