<?php

namespace App\Repositories\Interfaces;

interface PromotionRepositoryInterface
{
    /**
     * @return mixed
     */
    public function findByCode($code);
}
