<?php

namespace App\Repositories\Interfaces;

interface OrderRepositoryInterface
{
    /**
     *
     * @return mixed
     */
    public function record($data);
}
