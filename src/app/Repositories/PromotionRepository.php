<?php

namespace  App\Repositories;

use  App\Repositories\Interfaces\PromotionRepositoryInterface;
use App\Models\Promotion;

class PromotionRepository implements PromotionRepositoryInterface
{
    /**
     * Get's all class packs.
     *
     * @return mixed
     */
    public function findByCode($code)
    {
        return Promotion::where('code', $code)->first();
    }
}
