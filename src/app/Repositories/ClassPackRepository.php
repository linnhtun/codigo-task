<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ClassPackRepositoryInterface;
use App\Models\ClassPack;

class ClassPackRepository implements ClassPackRepositoryInterface
{

    /**
     * Get's all class packs.
     *
     * @return mixed
     */
    public function all()
    {
        return ClassPack::all();
    }
    /**
     * Get's paginated class packs.
     *
     * @return mixed
     */
    public function paginate($limit)
    {
        return ClassPack::paginate($limit);
    }
}
