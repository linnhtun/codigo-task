<?php

namespace App\Repositories;

use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Models\Order;


class OrderRepository implements OrderRepositoryInterface
{
    /**
     * Get's all class packs.
     *
     * @return mixed
     */
    public function record($data)
    {
        $order = new Order;

        $order->pack_id = $data['pack_id'];
        if (!empty($data['promotion_id'])) {
            $order->promotion_id = $data['promotion_id'];
        }

        return $order->save();
    }
}
