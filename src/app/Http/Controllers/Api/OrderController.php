<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $order;

    /**
     * PromotionController constructor.
     *
     * @param OrderRepositoryInterface $order
     */
    public function __construct(OrderRepositoryInterface $order)
    {
        $this->order = $order;
    }

    /**
     *
     * @return mixed
     */
    public function record(Request $request)
    {
        return response()->json($this->order->record($request->all()), 200);
    }
}
