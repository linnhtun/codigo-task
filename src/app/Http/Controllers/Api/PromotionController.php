<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\PromotionRepositoryInterface;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    protected $promotion;

    /**
     * PromotionController constructor.
     *
     * @param PromotionRepositoryInterface $promotion
     */
    public function __construct(PromotionRepositoryInterface $promotion)
    {
        $this->promotion = $promotion;
    }


    /**
     *
     * @return mixed
     */
    public function findByCode(Request $request)
    {
        return response()->json($this->promotion->findByCode($request->query('code')), 200);
    }
}
