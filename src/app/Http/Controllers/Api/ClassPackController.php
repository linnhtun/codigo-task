<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ClassPackRepositoryInterface;

class ClassPackController extends Controller
{
    protected $classPack;

    /**
     * ClassPackController constructor.
     *
     * @param ClassPackRepositoryInterface $classPack
     */
    public function __construct(ClassPackRepositoryInterface $classPack)
    {
        $this->classPack = $classPack;
    }


    /**
     * List all class packs.
     *
     * @return mixed
     */
    public function index()
    {
        $data = [];
        try {
            $packPaginate = $this->classPack->paginate(6);
            $data = [
                'errorCode' => 0,
                'message' => 'Success',
                'data' => [
                    'total_item' => $packPaginate->total(),
                    'total_page' => $packPaginate->lastPage(),
                    'mem_tier' => auth()->guard('api')->user()->tier,
                    'total_expired_class' => 0,
                    'pack_list' => $packPaginate->items(),
                ]
            ];
        } catch (\Exception $ex) {
            $data = [
                'errorCode' => $ex->getCode(),
                'message' => $ex->getMessage(),
                'data' => []
            ];
        }

        return response()->json($data, 200);
    }
}
