<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $guarded = ['id'];

    public $timestamps = true;

    protected $hidden = ['created_at', 'updated_at'];
}
