<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public $timestamps = true;

    protected $hidden = ['id', 'created_at', 'updated_at'];
}
