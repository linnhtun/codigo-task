<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Interfaces\ClassPackRepositoryInterface',
            'App\Repositories\ClassPackRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\PromotionRepositoryInterface',
            'App\Repositories\PromotionRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\OrderRepositoryInterface',
            'App\Repositories\OrderRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
