<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_packs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('disp_order', false, true);
            $table->uuid('pack_id');
            $table->string('pack_name', 500);
            $table->string('pack_description', 1000);
            $table->enum('pack_type', ['unlimited', 'shareable', 'non_shareable']);
            $table->integer('total_credit', false, true);
            $table->string('tag_name', 100)->nullable();
            $table->integer('validity_month', false, true);
            $table->decimal('pack_price', 10);
            $table->boolean('newbie_first_attend');
            $table->integer('newbie_addition_credit', false, true);
            $table->string('newbie_note', 1000);
            $table->string('pack_alias', 50);
            $table->decimal('estimate_price', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_packs');
    }
}
