<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 3) as $index) {
            $timestamp = $faker->dateTime();
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('password'),
                'tier' => $faker->randomElement(['newbie', 'pro']),
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ]);
        }
    }
}
