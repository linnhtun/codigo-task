<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ClassPacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 12) as $index) {
            $timestamp = $faker->dateTime();
            $credit = $faker->numberBetween(0, 11) * 10;
            $packType = $credit === 0 ? 'unlimited' : $faker->randomElement(['shareable', 'non_shareable']);
            $packPrice = $faker->randomFloat(2);
            DB::table('class_packs')->insert([
                'disp_order' => $index,
                'pack_id' => $faker->uuid,
                'pack_name' => ($credit === 0 ? 'Unlimited Class ' : ($credit === 110 ? 'Single Class ' : $credit . ' Class Pack ')) . $index,
                'pack_description' => $faker->text(100),
                'pack_type' => $packType,
                'total_credit' => $credit === 110 ? 1 : $credit,
                'tag_name' => $faker->randomElement([null, 'New', 'Popular', 'Limited']),
                'validity_month' => (int) $faker->month(),
                'pack_price' => $packPrice,
                'newbie_first_attend' => $faker->boolean(),
                'newbie_addition_credit' => $faker->numberBetween(0, 1),
                'newbie_note' => $faker->text(100),
                'pack_alias' => $credit === 0 ? 'unlimited-pack' : ($credit === 110 ? 'single-pack' : 'pack-' . $credit),
                'estimate_price' => $credit === 0 ? 0 : round($packPrice / $credit, 2),
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ]);
        }
    }
}
