<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 12) as $index) {
            $timestamp = $faker->dateTime();
            DB::table('promotions')->insert([
                'code' => $faker->word(10),
                'percent' => $faker->numberBetween(10, 40),
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ]);
        }
    }
}
