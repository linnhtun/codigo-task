FROM php:7.2-fpm-alpine

RUN apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS libxml2-dev

RUN docker-php-ext-install pdo pdo_mysql mbstring bcmath ctype json tokenizer xml \
    && curl -sSL https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer